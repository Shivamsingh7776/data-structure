# Data Structures:-
## List []:
Lists are used to store multiple items in a single variable.

Lists are one of 4 built-in data types in Python used to store collections of data, the other 3 are Tuple, Set, and Dictionary, all with different qualities and usage.

Lists are created using square brackets:

Example:- 
```python
    list_data = ['Shivam','Harsh','Ritesh','Zaid','Rayyan']
    print(list_data)
```
output :- ['Shivam','Harsh','Ritesh','Zaid','Rayyan']


### List Items :
List items are ordered, changeable, and allow duplicate values.

List items are indexed, the first item has index [0], the second item has index [1] etc.

Example:-
```python
list_data = ['Shivam','Harsh','Ritesh','Zaid','Rayyan']
          print(list_data[0])
```
output:- Shivam

### Changeable :
The list is changeable, meaning that we can change, add, and remove items in a list after it has been created.

Example:-
```python
list_data = ['Shivam','Harsh','Ritesh','Zaid','Rayyan']
         list_data[1] = 'Rahul'
```
output :- list_data = ['Shivam','Rahul','Ritesh','Zaid','Rayyan']

### Allow Duplicates:-
Since lists are indexed, lists can have items with the same value.
Example:- 
```python
list_data = ['Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']
          print(list_data)
```
output:- ['Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']
### List Items-DataTypes:-
List items can be of any data type:
Example :- 
```python
 list1 = ["apple", "banana", "cherry"]
            list2 = [1, 5, 7, 9, 3]
            list3 = [True, False, False]
```
A list can contain different data types:
Example:- 
``` python
 list4 = ["abc",54,True, 90 ,"xyz"]
            print(list4)
```
output:- ["abc",54,True, 90 ,"xyz"]

### type():-
Example:-
```python
 list_data = ['Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']
          print(type(list_data))
```
output:- <class 'list'> 

### The list() Constructor:-
using the list() constructor to make a List:

Example:- 
```python
list_data = list(('Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid'))
          print(list_data)
```
output:- ['Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']

### Access Items of the List:-
List items are indexed and you can access them by referring to the index number.
Example:- 
```python
list_data = ['Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']
          print(list_data[0])
```
output:-  Shivam

### Negative Indexing:-
Negative indexing means start from the end
-1 refers to the last item, -2 refers to the second last item etc.

Example:-
```python
list_data = ['Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']
         print(list_data[-1])
```
output:- Zaid

### Range of Indexes:-
You can specify a range of indexes by specifying where to start and where to end the range.
When specifying a range, the return value will be a new list with the specified items.
Example:-
```python
 list_data = ['Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']
          print(list_data[2:5])
```
output :- ['Ritesh','Zaid','Rayyan']
```python
          print(list_data[:4])
```
output :- ['Shivam','Harsh','Ritesh','Zaid]
```python
          print(list_data[2:])
```
output :- ['Ritesh','Zaid','Rayyan','Shivam','Zaid']

### More on Lists:-
The list data type has some more methods like,
#### list_data.append(x):-
Add an items to the end of the list.

Example:-
```python
 list_data = ['Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']
          print(list_data.append('Kalyan'))
```
output:- ['Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid','Kalyan']

### list.insert(i,x):-
Insert an item at a given position. The first argument is the index of the element before which to insert, so a.insert(0, x) inserts at the front of the list, and a.insert(len(a), x) is equivalent to a.append(x).

Example:-
```python
 list_data = ['Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']
          list_data.insert(1,'Krishan')
          print(list_data)
```
output:- ['Shivam','Krishna','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']

###  list_data.remove(x):-
Remove the first item from the list whose value is equal to x. It raises a ValueError if there is no such item.
Example:-
```python
list_data = ['Shivam','Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']
         list_data.remove('shivam')
         print(list_data)
```
output:- ['Harsh','Ritesh','Zaid','Rayyan','Shivam','Zaid']

### list_data.pop([i]):-
Remove the item from the given index in the list, and return it. when we'll not give any index then it will remove last item, return that.
Example:-
```python
list_data = [1,2,3,4,5,6,7,8]
list_data.pop(2)
print(list_data)
```
output:- [1, 2, 4, 5, 6, 7, 8]
```python
         list_data.pop()
         print(list_data)
```
output:- [1, 2, 4, 5, 6, 7]

### list_data.clear():-
Remove all items from the list. 
Example:-
```python
list_data = [1,2,3,4,5,6,7,8]
         list_data.clear()
         print(list_data)
```
output:- []
### list_data.index():-
Return the position of item there that present in case of duplcate it'll give the first occurance of index.
Example:-
```python
 list_data = [1,2,2,3,4,5,6,7,8]
          print(list_data.index(2))
```
output:- 1

### list_data.count(x):-
Return the number of times x appears in the list.
Example:-
```python
  list_data = [1,2,2,3,4,5,6,7,8]
           print(list_data.count(2))
```

output:- 2
### list_datas.sort():-
It will the sort the numbers in increasing order and string using that alphabatical charactor.
Example:- 
```python
fruits = ['orange', 'apple', 'pear', 'banana', 'kiwi', 'apple', 'banana']
          (fruits.sort())
           print(fruits)
```
output:- ['apple', 'apple', 'banana', 'banana', 'kiwi', 'orange', 'pear']

### list_data.reverse():-
Reverse the elements of the list in place.
Example:- 
```python
 fruits = ['orange', 'apple', 'pear', 'banana', 'kiwi', 'apple', 'banana']
           (fruits.reverse())
            print(fruits)
```
output:- ['banana', 'apple', 'kiwi', 'banana', 'pear', 'apple', 'orange']

###  list.copy():-
Return the copy of string .

Example:- 
```python
fruits = ['orange', 'apple', 'pear', 'banana', 'kiwi', 'apple', 'banana']
          my_fruit = fruits.copy()
          print(my_fruit)
```
output:-['orange', 'apple', 'pear', 'banana', 'kiwi', 'apple', 'banana']

### Using Lists as Stacks:-
Stacks is a linear data structure which follows a particular order in which the operation are performed. The order may be LIFO(Last in First Out) or FILO (First in Last Out).
List make it easy to use a list as stack, where the last element added is the first element retrieved .To add an item to the top of the stack, use append(). To retrieve an item from the top of the stack, use pop().

Example:-
``` python
stack = [1,2,4,5]
          stack.append(7)
          print(stack)
```
output:-[1, 2, 4, 5, 7]
```python
          stack.append(8)
          stack.append(9)
          print(stack)
```
output:-[1, 2, 4, 5, 7,8,9]
```python
         stack.pop()
```
output:- [1, 2, 4, 5, 7,8]
```python
         stack.pop()
         stack.pop()
```
output:-[1, 2, 4, 5]



### Using List as Queues:-
Queue is FIFO data structure,it is also possible to use list as queue,where first element added is the first element retrieved.However lists are not efficient for this purpuse.While appends and pops from the end of the list are fast,doing inserts or pops from the beginning of a list is slow.
To implementing a queue,use collections.deque which was desined to have fast appends and pops from both ends.
Example:- 
```python
from collections import deque
          queue = deque(["Shivam","John","Michael"])
          queue.append("Rayyan")
          queue.append("Ritesh")
          print(queue)
  
output:- deque(['Shivam', 'John', 'Michael', 'Rayyan', 'Ritesh'])
         queue.popleft()
         queue.popleft()
         print(queue)
output:-deque(['Michael', 'Rayyan', 'Ritesh'])
```

### List Comprehensions:-
List comprehensions provide a concise way to create lists. Common applications are to make new lists where each element is the result of some operations applied to each member of another sequence or iterable, or to create a subsequence of those elements that satisfy a certain condition.
Example:-
```python
 list_data = []
          for i in range(10):
              list_data.append(i**2)

           print(list_data)
```
output:- [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
->Whithout using any side effects like by creating empty list
Example:-
```python
 list_data = list(map(lambda i : i**2,range(10)))
          print(list_data)
```
output:-[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
                    OR
Example:-
```python
list_data = [i**2 for i in range(10)]
         print(list_data)
```
output:- [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
->If there one more varibles in the list
Example:-
```python
list_data=[(x, y) for x in [1,2,3] for y in [3,1,4] if x != y]
         print(list_data)
```
output:-[(1, 3), (1, 4), (2, 3), (2, 1), (2, 4), (3, 1), (3, 4)]
                     OR
Example:-
```python
list_data = []
         for i in [1,2,3]:
            for j in [3,1,4]:
                if i != j:
                  list_data.append((i,j))
print(list_data)
```
output:-[(1, 3), (1, 4), (2, 3), (2, 1), (2, 4), (3, 1), (3, 4)]

->Using one list creating another list
Example:-
```python
>>>list_data = [9,-2,-8,1,3,4]
         >>>[x*2 for x in list_data]
```
output:- [18,-4,-16,2,6,8]
Example:-
```python
 >>>[i for i in list_data if i >= 0]
```
output:- [1,2,3,4,8]
->Make all element POsitive in the list
Example:-
```python
>>[abs(x) for x in list_data]
```
output:-    [9,2,8,1,3,4]
->call a method on each element
Example:-
```python
 >>> fruit = ['Banana','Apple','Orange']
          >>> [x.strip() for x in fruit]

```
output:-   ['Banana','Apple','Orange']

--> Creating a list of 2-tuple like (number,square)
Example:-
  ```python
 >>> [(x,x**2) for x in range(6)]
 ```
output:- [(0, 0), (1, 1), (2, 4), (3, 9), (4, 16), (5, 25)]

-->the tuple must be parenthesized, otherwise an error is raised

Example:-
```python
[x, x**2 for x in range(6)]
```
  File "<stdin>", line 1
    [x, x**2 for x in range(6)]
     ^^^^^^^
SyntaxError: did you forget parentheses around the comprehension target?

--> List comprehensions can contain complex expressions and nested functions:
Example:-
```python
 from math import pi
          [str(round(pi,i)) for i in range(1,6)]
```
output:- ['3.1', '3.14', '3.142', '3.1416', '3.14159']

### Nested Lits Comprehensions:-
The initial expression in a list comprehension can be any arbitrary expression, including another list comprehension.
Example:-
```python
 mat = [
    [1,2,3,4],
    [5,6,7,8],
    [9,10,11,12],
  ] 
```
--> For finding Transpose
Example:- 
```python
[[row[i] for row in mat for i in range(4)]
```
output:-[[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]]
                  OR
Example:-
```python
 transposed = []
          for i in range(4):
             transposed.append([row[i] for row in matrix])
           print(transposed)
```
output:-[[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]]

--> Using Zip()
Example:-
```python
list(zip(*mat))

```
output:-[(1, 5, 9), (2, 6, 10), (3, 7, 11), (4, 8, 12)]

### Del Satement:-
This a way to remove an item from a list given its index instead of its value.This is differ from pop() method,because pop()method return removed value.
Example:-
```python
 x = [-1, 1, 66.25, 333, 333, 1234.5]
          del x[0]
          print(x)
```
output:[-1, 66.25, 333, 333, 1234.5]
Example:- 
```python
del x[2:4]
  print(x)
```
output:- []
--> For deleting entire variable 
Example:- 
```python
del x
```
output:- []


## Tuples ():-
Tuples are used to store multiple items in a single variable.A tuple is a collection which is ordered and unchangeable.
Tuples are written with round brackets.
Example:- 
```python
tup_data = ("Shivam","Zaid","Ritesh")
          print(tup_data)
```
output:- ('Shivam', 'Zaid', 'Ritesh')

### Tuple Items:-
Tuple items are ordered, unchangeable, and allow duplicate values.
Tuple items are indexed, the first item has index [0], the second item has index [1] etc.
Example:-
```python
tup_data = ("Shivam","Zaid","Ritesh")
         print(tup_data[0])
```
output:- Shivam

### Ordered:-
When we say that tuples are ordered, it means that the items have a defined order, and that order will not change.

### Allow Duplicates:-
they can have items with the same value.
Example:-
```python
tup_data = ("Shivam","Zaid","Ritesh","Shivam","Ritesh)
         print(tup_data)
```
output:- ('Shivam', 'Zaid', 'Ritesh', 'Shivam', 'Ritesh')

### Tuples Items-Data Types:-
Tuple items can be of any data types:
Example:-
```python
 tuple1 = ("apple", "banana", "cherry")
          tuple2 = (1, 5, 7, 9, 3)
          tuple3 = (True, False, False)
          print(tuple1)
          print(tuple2)
          print(tuple3)
```
output:- ('apple', 'banana', 'cherry')
         (1, 5, 7, 9, 3)
         (True, False, False)
Example:-
```python
tuple1 = ("abc", 34, True, 40, "male")
         print(tuple1)
```
output:-('abc', 34, True, 40, 'male') 

--> Tuple may nested 
Example:-
``` python
>>> t = (1,2,3,4,5)
          u = t,(6,7,8,910)
          print(u)
```
output:-((1,2,3,4,5),(6,7,8,9,10))
--> Tuples are immutable,we cannot add or remove element of Tuples
Example:-
```python
  t[0] = 12
```
Error:-Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment

-->But they can contain mutable objects.
Example:-
```python
 x = ([1,2,3,4],([4,3,2,1])
          print(x)
```
output:-([1, 2, 3, 4], [4, 3, 2, 1])

## Sets {}:-
Sets are used to store multiple items in a single variable.A set is a collection which is unordered, unchangeable*, and unindexed.Set contain non-duplicate element only.when we are passing duplcate element then it make that unique.

Example:-
```python
set_data = {"Shivam","Zaid","Ritesh","Shivam"}
         print(set_data)
```
output:-{'Zaid', 'Ritesh', 'Shivam'}

### Set Items:-
Set items are unordered, unchangeable, and do not allow duplicate values.

### Unordered:-
Unordered means that the items in a set do not have a defined order.
Set items can appear in a different order every time you use them, and cannot be referred to by index or key.
Example:-
```python
set_data{"Shivam","Zaid","Ritesh","Shivam"}
         print(set_data)
```
output:-{'Zaid','Shivam','Ritesh'}

### Unchangeable:-
Set items are unchangeable, meaning that we cannot change the items after the set has been created.
Example:-
```python
set_data{"Shivam","Zaid","Ritesh","Shivam"}
         set_data[1] = "Harsh"
         print(set_data)
```
output:- Traceback (most recent call last):
  File "./prog.py", line 2, in <module>
TypeError: 'set' object does not support item assignment

### Sets Items - Data types:-
Set items can be of any data type.
Example:-
```python
 set1 = {"apple", "banana", "cherry"}
          set2 = {1, 5, 7, 9, 3}
          set3 = {True, False, False}
          print(set1)
          print(set2)
          print(set3)
```
output:- {'cherry', 'apple', 'banana'}
         {1, 3, 5, 7, 9}
         {False, True}
Example:- 
```python
set1 = {"abc", 34, True, 40, "male"}
          print(set1)
```
output:- {True, 34, 40, 'male', 'abc'} 

Example:-
```python
 a = set('abracadabra')
          b = set('alacazam')
          print(a-b)
```
output:- {'r','d','b'}

Example:-
```python
 print(a|b) #Letter in a or b or both
```

output:-{'a', 'c', 'r', 'd', 'b', 'm', 'z', 'l'}

Example:-
```python
 print(a&b) # letter in both
```
output:- {'a','c'}

### Set Comrehensions:-
Similarly to list comprehensions, set comprehensions are also supported.

Example:-
```python
 x = {a for a in 'abracadabra' if a not in 'abc'}
          print(x)
```
output:-{'d', 'r'}


## Dictionaries {kye:value}:-
Dictionaries are used to store data values in key:value pairs.A dictionary is a collection which is ordered*, changeable and do not allow duplicates.

Example:-
```python
 dict_data = {
    "Shivam" : " C++",
    "Zaid"   :  "Java",
    "Ritesh" :  "Python",
    "Harsh "  :   "C"
}
print(dict_data)
```
output:- {'Shivam': ' C++', 'Zaid': 'Java', 'Ritesh': 'Python', 'Harsh ': 'C'}

### Dictionary Items:-
Dictionary items are ordered, changeable, and does not allow duplicates.
Dictionary items are presented in key:value pairs, and can be referred to by using the key name.
Example:-
```python
dict_data = {
    "Shivam" : " C++",
    "Zaid"   :  "Java",
    "Ritesh" :  "Python",
    "Harsh "  :   "C"
}
print(dict_data["Shivam"])
```
output:-  C++


### Ordered or Unordered:-
As of Python version 3.7, dictionaries are ordered. In Python 3.6 and earlier, dictionaries are unordered.
When we say that dictionaries are ordered, it means that the items have a defined order, and that order will not change.
Unordered means that the items does not have a defined order, you cannot refer to an item by using an index.

### Changeable:-
Dictionaries are changeable, meaning that we can change, add or remove items after the dictionary has been created.
Example:-
``` python
dict_data = {
    "Shivam" : " C++",
    "Zaid"   :  "Java",
    "Ritesh" :  "Python",
    "Harsh "  :   "C"
}
dict_data["Krishna"] = "JavaScript"
```
output:- {'Shivam': ' C++', 'Zaid': 'Java', 'Ritesh': 'Python', 'Harsh ': 'C', 'Krishna': 'Javascript'}

### Duplicates:-
Duplicates are not allowed,Dictionaries cannot have two items with the same key.

Example:-
```python
dict_data = {
    "Shivam" : " C++",
    "Zaid"   :  "Java",
    "Ritesh" :  "Python",
    "Harsh " :   "C"
    "Zaid"   :   "Python"
}
print(dict_data)
```
output:- it will give error ,because one key contains multiple values.

### Accessing Items:-
You can access the items of a dictionary by referring to its key name, inside square brackets:
Example:-
```python
dict_data = {
    "Shivam" : " C++",
    "Zaid"   :  "Java",
    "Ritesh" :  "Python",
    "Harsh "  :   "C"
}
print(dict_data["Ritesh"])
```
output:- Python

-->There is also a method called get() that will give you the same result.
Example:- 
```python
dict_data = {
    "Shivam" : " C++",
    "Zaid"   :  "Java",
    "Ritesh" :  "Python",
    "Harsh " :   "C"
   
}
x = dict_data.get("Ritesh")
print(x)
```
output:-Python

### Get Keys:-
The keys() method will return a list of all the keys in the dictionary.
Example:-
``` python
dict_data = {
    "Shivam" : " C++",
    "Zaid"   :  "Java",
    "Ritesh" :  "Python",
    "Harsh " :   "C"
   
}
x = dict_data.keys()

print(x)
```

output:- dict_keys(['Shivam', 'Zaid', 'Ritesh', 'Harsh '])


### Loop in Dictionary:-
We can loop through a dictinary, and it'll return the key value.
Example:- 
```python
for x in dict_data:
   print(x,end = '')
```
Output:-Shivam Zaid ,Ritesh, Harsh
Example:- For return Vlues
```python
for x in dict_data:
   print(dict_data[x],end= '')
```
output:- C++ Java Python C

### Copy a Dictionary:-
For copy one dictionary to other we use copy() method 
Exampe:- 
```python
dict_data = {
    "Shivam" : " C++",
    "Zaid"   :  "Java",
    "Ritesh" :  "Python",
    "Harsh " :   "C"
   
}
my_dict = dict_data.copy()
print(my_dict)
```
Output:-{'Shivam': ' C++', 'Zaid': 'Java', 'Ritesh': 'Python', 'Harsh ': 'C'}

### Nested Dictionaries:-
A dictionary can conation lots of dictnaries inside that.
Example:-
```python
myfamily = {
  "child1" : {
    "name" : "Emil",
    "year" : 2004
  },
  "child2" : {
    "name" : "Tobias",
    "year" : 2007
  },
  "child3" : {
    "name" : "Linus",
    "year" : 2011
  }
}

print(myfamily)

```
output:- {'child1': {'name': 'Emil', 'year': 2004}, 'child2': {'name': 'Tobias', 'year': 2007}, 'child3': {'name': 'Linus', 'year': 2011}}

Example:-

```python
child1 = {
  "name" : "Emil",
  "year" : 2004
}
child2 = {
  "name" : "Tobias",
  "year" : 2007
}
child3 = {
  "name" : "Linus",
  "year" : 2011
}

myfamily = {
  "child1" : child1,
  "child2" : child2,
  "child3" : child3
}

print(myfamily)
```
Output:-{'child1': {'name': 'Emil', 'year': 2004}, 'child2': {'name': 'Tobias', 'year': 2007}, 'child3': {'name': 'Linus', 'year': 2011}}

### Dictionary Methods:-
#### 1.clear() 
Removes all the elements from the dictionary.
Example:-
```python
   car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

car.clear()

print(car)
```
output:-{}
#### 2.copy()
Returns a copy of the dictionary.
Example:-
```python
   car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.copy()

print(x)

```
output:-{'brand': 'Ford', 'model': 'Mustang', 'year': 1964} 
#### 3.fromkeys()
Returns a dictionary with the specified keys and value.
Example:-
```python
   x = ('key1', 'key2', 'key3')
   y = 1
   thisdict = dict.fromkeys(x, y)
   print(thisdict)
```
output:-'key1': 1, 'key2': 1, 'key3': 1] 
#### 4.get()
Returns the value of the specified key.
Example:-
```python
  car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
x = car.get("model")
print(x) 
```
output:-Mustang 
#### 5.items()
Returns a list containing a tuple for each key value pair.
Example:-
```python
  car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
x = car.items()
print(x) 
```
output:- dict_items([('brand', 'Ford'), ('model', 'Mustang'), ('year', 1964)]) 
#### 6.keys()
Returns a list containing the dictionary's keys.
Example:-
```python
car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
x = car.keys()
print(x) 
```
output:-dict_keys(['brand', 'model', 'year']) 
#### 7.pop()
Removes the element with the specified key.
Example:-
```python
   car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
car.pop("model")
print(car)
```
output:-{'brand': 'Ford', 'year': 1964}
#### 8.popitem()
Removes the last inserted key-value pair.
Example:-
```python
car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
car.popitem()
print(car)
```
output:-{'brand': 'Ford', 'model': 'Mustang'} 
#### 9.update()
Updates the dictionary with the specified key-value pairs.
Example:-
```python
car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
car.update({"color": "White"})
print(car) 
```
output:-{'brand': 'Ford', 'model': 'Mustang', 'year': 1964, 'color': 'White'} 
#### 10.values()
Returns a list of all the values in the dictionary.
Example:-
```python
car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
x = car.values()
print(x) 
```
output:- dict_values(['Ford', 'Mustang', 1964]) 













        












          






